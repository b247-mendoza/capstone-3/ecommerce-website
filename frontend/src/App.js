import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Header from './components/Header';
import Footer from './components/Footer';
import Homepage from './pages/Homepage';
import Productpage from './pages/Productpage';
import Cartpage from './pages/Cartpage';
import Loginpage from './pages/Loginpage';
import Registerpage from './pages/Registerpage';
import Profilepage from './pages/Profilepage';

const App = () => {
  return (
    <Router>
      <Header />
      <main className="py-3">
        <Container>
          <Routes>
            <Route path="/login" Component={Loginpage} exact />
            <Route path="/register" Component={Registerpage} exact />
            <Route path="/profile" Component={Profilepage} exact />
            <Route path="/product/:id" Component={Productpage} />
            <Route path="/cart/:id?" Component={Cartpage} exact />
            <Route path="/" Component={Homepage} exact />
          </Routes>
        </Container>
      </main>
      <Footer />
    </Router>
  );
};

export default App;
