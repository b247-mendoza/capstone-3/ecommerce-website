import bcrypt from 'bcryptjs';

const users = [
  {
    name: 'Admin User',
    email: 'admin@mail.com',
    password: bcrypt.hashSync('12345678', 10),
    isAdmin: true,
  },
  {
    name: 'Customer User1',
    email: 'userone@mail.com',
    password: bcrypt.hashSync('12345678', 10),
  },
  {
    name: 'Customer User2',
    email: 'usertwo@mail.com',
    password: bcrypt.hashSync('12345678', 10),
  },
];

export default users;
